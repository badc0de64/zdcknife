﻿/*
 * Created by SharpDevelop.
 * User: DZC1KAZ
 * Date: 12.12.2015
 * Time: 12:52
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ZDCknife;

namespace ZDCknife.Classes
{
	/// <summary>
	/// Класс определен как partial, чтобы вынести вспомогательные функции в отдельный файл дабы не засорять основной листинг.
	/// </summary>
	public partial class ZDCConverter
	{
		
		// Очистить строку от ненужных символов, привести к верхнему регистру и сформировать массив
		private string[] CleanAndSplitString(string inputStr)
		{		
			if (!String.IsNullOrEmpty(inputStr))
			{
				// Оставить в строке только буквы и цифры
				string ClearStr = Regex.Replace(inputStr, @"[^a-zA-Z0-9]", "");
				// Возьмем максимально возможную длину строки, которая делится на 3 без остатка
				int StrSize = (int)(ClearStr.Length / 3);
				// Преобразуем строку в строковый массив по 3 символа в каждой строке
				string[] ResultStrArray = new string[StrSize];
				for (int i = 0; i < StrSize; i++)
				{
					ResultStrArray[i] = ClearStr.Substring(i * 3, 3).ToUpper();
				}
				return ResultStrArray;
			}
			// Если входная строка null или пустая, то вернуть null
			return null;
		}
		
		// Вычислить актуальный массив PR-чисел для автомобиля
		private string[] CalculateCarPRNumbers(Dictionary<string,string[]> ZDCNumbers, string[] InputPRNumbersArray)
		{
			// Получаем коллекцию ключей.
			Dictionary<string, string[]>.KeyCollection FAMKeys = ZDCNumbers.Keys;
			// Результирующий массив строк.
			string[] ResultPRNumbersArray = new string[FAMKeys.Count];
			// Индекс для результирующего массива
			int index = 0;
				
			if ((ZDCNumbers != null) & (InputPRNumbersArray != null))
			{
				foreach (string FAMKey in FAMKeys)
				{
					// Переменная для контроля количества PR-чисел для каждой семьи(FAMKey).
					// Для одной семьи может быть только одно PR-число.
					int PRCountControl = 0;
					string[] tempPRNumbersList;
					if (ZDCNumbers.TryGetValue(FAMKey, out tempPRNumbersList))
					{
						foreach (string PRNum in tempPRNumbersList) 
						{
							foreach (string InputPRNum in InputPRNumbersArray) 
							{
								if(PRNum == InputPRNum)
								{
									PRCountControl++;
									// Если данные содержат больше одного PR-числа для одной фамилии, то вывести сообщение и выйти из функции
									if (PRCountControl > 1)
									{
										MessageWindow.Show("Car data contains a few (or several identical) PR-numbers for Family " + FAMKey, "CalculateCarPRNumbers exception");
										return null;
									}
									ResultPRNumbersArray[index] = PRNum;
									index++;
								}
							}
						}
						// Если данные не содержат PR-числа для одной фамилии, то вывести сообщение и выйти из функции
						if (PRCountControl == 0)
						{
							MessageWindow.Show("Car data doesn't contain any PR-number for Family " + FAMKey + "." + Environment.NewLine +
							                   "Check PR-numbers or try another ZDC-file.", "CalculateCarPRNumbers exception");
							return null;
						}
					}
					else
					{
						MessageWindow.Show("ZDC dictionary doesn't contain the key " + FAMKey, "CalculateCarPRNumbers exception");
						return null;
					}
				}
				return ResultPRNumbersArray;
			}
			// Вернуть null, если входные параметры null
			return null;
		}
		
		// Метод для сравнения строки PR-чисел из контейнера и PR-чисел автомобиля.
		private bool CheckPRNumbers(string strPRNumbers)
		{
			if (String.IsNullOrEmpty(strPRNumbers))
				throw new Exception("Input string cannot be null or empty!");
			if (CarPRNumbers == null)
				throw new Exception("List of the car's PR-numbers cannot be null!");
			// Удалить "+" в начале строки, так как иногда в ZDC-контейнерах PR числа пишутся с плюсом в начале.
			// Например +7IX. В результате проверка получается некорректной.
			char[] symbolsToDelete = {'+'};
			strPRNumbers = strPRNumbers.TrimStart(symbolsToDelete);
			
			// Пример входной строки: 1MS/2PB+8T2+9Q0
			string[] FamPRNumbers = strPRNumbers.Split('+');
			// Эта переменные нужны для определения, содержит ли строка все необходимые PR-числа.
			int PRArrayLength = FamPRNumbers.Length;
			// Например, PRNumsCountControl в итоге должно быть равно 3 (1MS/2PB+8T2+9Q0).
			int PRNumsCountControl = 0;
			foreach (string strFamPRNums in FamPRNumbers)
			{
				string[] PRNumbersList = strFamPRNums.Split('/');
				foreach (string PRNum in PRNumbersList)
				{
					foreach (string CarPRNum in CarPRNumbers)
					{
						// Если такое PR-число есть в PR-числах автомобиля, то увеличить на 1 контрольную переменную.
						if (PRNum == CarPRNum)
							PRNumsCountControl++;
					}
				}
			}
			// Сравнить рассчитанную контрольную переменную и число, которое мы должны получить(PRArrayLength).
			// Если они не равны, то данная строка strPRNumbers не подходит для расчета кодировки.
			return PRNumsCountControl == PRArrayLength;
		}
		
		// Метод рассчитывает значение байта на основе позиции бита и значения этого бита (0 или 1).
		private byte CalculateByte(byte BitPosititon, byte BitValue)
		{
			// Формируем выходной байт путем битового сдвига влево значения BitValue на BitPosititon сдвигов.
			byte ResultByte = BitValue;
			return ResultByte = (byte)(ResultByte << BitPosititon);
		}
		
		// Метод для получения строки с длиной данных для программирования в случае, когда вместо WERT есть DATEN-NAME.
		private string GetDataLengthString(string DataName)
		{
			try
			{
				if (ZDCFile.Descendants().Any(node => node.Name == "DATENBEREICH"))
				{
					IEnumerable<XElement> DataList = ZDCFile.Root.Element("VORSCHRIFT").Element("DIREKT").Element("DATENBEREICHE").Elements("DATENBEREICH");
					foreach (XElement DataNode in DataList)
					{
						if(DataNode.Element("DATEN-NAME").Value == DataName)
						{
							return DataNode.Element("GROESSE-DEKOMPRIMIERT").Value;
						}
					}
				}
				return null;
			}
			catch
			{
				return null;
			}
		}
		
		// Метод для получения строки с длиной данных для программирования в случае, когда вместо WERT есть DATEN-NAME.
		private string GetDataFormatString(string DataName)
		{
			try
			{
				if (ZDCFile.Descendants().Any(node => node.Name == "DATENBEREICH"))
				{
					IEnumerable<XElement> DataList = ZDCFile.Root.Element("VORSCHRIFT").Element("DIREKT").Element("DATENBEREICHE").Elements("DATENBEREICH");
					foreach (XElement DataNode in DataList)
					{
						if(DataNode.Element("DATEN-NAME").Value == DataName)
						{
							return DataNode.Element("DATEN-FORMAT-NAME").Value;
						}
					}
				}
				return null;
			}
			catch
			{
				return null;
			}
		}
		
		// Метод для получения строки с данными для программирования в случае, когда вместо WERT есть DATEN-NAME.
		private string GetPRGString(string DataName)
		{
			try
			{
				if (ZDCFile.Descendants().Any(node => node.Name == "DATENBEREICH"))
				{
					IEnumerable<XElement> DataList = ZDCFile.Root.Element("VORSCHRIFT").Element("DIREKT").Element("DATENBEREICHE").Elements("DATENBEREICH");
					foreach (XElement DataNode in DataList)
					{
						if(DataNode.Element("DATEN-NAME").Value == DataName)
						{
							return DataNode.Element("DATEN").Value;
						}
					}
				}
				return null;
			}
			catch
			{
				return null;
			}
		}
		
		// Метод для получения стартового адреса для программирования в случае, когда вместо WERT есть DATEN-NAME.
		private int GetStartAddress(string DataName)
		{
			try
			{
				if (ZDCFile.Descendants().Any(node => node.Name == "DATENBEREICH"))
				{
					IEnumerable<XElement> DataList = ZDCFile.Root.Element("VORSCHRIFT").Element("DIREKT").Element("DATENBEREICHE").Elements("DATENBEREICH");
					foreach (XElement DataNode in DataList)
					{
						if(DataNode.Element("DATEN-NAME").Value == DataName)
						{
							return Convert.ToInt32(DataNode.Element("START-ADR").Value, 16);
						}
					}
				}
				return Int32.MinValue;
			}
			catch
			{
				return Int32.MinValue;
			}
		}
	}
}
