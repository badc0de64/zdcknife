﻿/*
 * Created by SharpDevelop.
 * User: DZC1KAZ
 * Date: 09.11.2015
 * Time: 13:54
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using ZDCknife;
using System.Diagnostics;


namespace ZDCknife.Classes
{	
	/// <summary>
	/// Класс, для работы с ZDC-файлами.
	/// Позволяет рассчитать кодировку,адаптацию,программирование блока управления на основе введеных PR-чисел автомобиля и ZDC.
	/// </summary>
	public partial class ZDCConverter
	{
		#region Public properties
		
		// Массив PR-чисел автомобиля.
		// TODO переименовать в PRNumbersReference
		public string[] CarPRNumbers { get; private set; }
		
		// Содержит ли файл данные для адаптации.
		public bool HasAdaptationData { get; private set; }
		// Содержит ли файл данные для кодирования.
		public bool HasCodingData { get; private set; }
		// Содержит ли файл данные для калибровки.
		public bool HasCalibrationData { get; private set; }
		// Содержит ли файл данные для программирования.
		public bool HasProgrammingData { get; private set; }
		
		#endregion
		
		#region Private fields
		
		// Хэндл ZDC-файла.
		XDocument ZDCFile;
		// Словарь фамилий и их PR-чисел, полученный из ZDC-файла. 
		Dictionary<string, string[]> ZDCFAMPRNumbers = new Dictionary<string, string[]>();
		// Дерево (TABELLEN) всех элементов TABELLE. 
		IEnumerable<XElement> TablesList;
		// Строка для форматирования в HEX.
		const string HexFormatString = "X4";
		
		#endregion
		
		// Class constructor
		public ZDCConverter(string ZDCFileName, string InputPRNumbersList)
		{
			try
			{					
				ZDCFile = XDocument.Load(ZDCFileName);
				
				IEnumerable<XElement> ZDCData = ZDCFile.Root.Element("VORSCHRIFT").Element("DIREKT").Element("TABELLEN").Element("REFERENZ").Elements("REFFAM");
				
				// Получим список всех таблиц ZDC-файла.
				TablesList = ZDCFile.Root.Element("VORSCHRIFT").Element("DIREKT").Element("TABELLEN").Elements("TABELLE");
				// Проверить какие данные присутствуют в файле.
				HasAdaptationData =  TablesList.Elements().Any(node => node.Name == "MODUS" & node.Value == "A");
				HasCodingData =      TablesList.Elements().Any(node => node.Name == "MODUS" & node.Value == "C");
				HasCalibrationData = TablesList.Elements().Any(node => node.Name == "MODUS" & node.Value == "K");
				HasProgrammingData = TablesList.Elements().Any(node => node.Name == "MODUS" & node.Value == "P");
				
				foreach (XElement REFFAM in ZDCData)
				{
					// Получим значение строки фамилии. Это значение будет ключом в словаре.
					string FAMKey = REFFAM.Element("FAMNR").Value;
					IEnumerable<XElement> PRNumList = from PRNR in REFFAM.Elements("PRNR") select PRNR;
					// Получим количество PR-чисел для данной фамилии. Это число будет размером будущего массива.
					string[] PRNumbers = new string[PRNumList.Count()];
					int PRCounter = 0;
					// Заполним массив значениями PR-чисел.
					foreach (XElement PRNR in PRNumList)
					{
						PRNumbers[PRCounter] = PRNR.Value;
						PRCounter++;
					}
					// Добавим массив в словарь.
					ZDCFAMPRNumbers.Add(FAMKey, PRNumbers);
					// Обнулим строку. Надеюсь, сборщик мусора уберет за мной :)
					PRNumbers = null;
				}
				
				// Очистить входную строку с данными на автомобиль и сделать массив строк.
				string[] CarPRNumbersArray = CleanAndSplitString(InputPRNumbersList);
				// Вычислим PR-числа автомобиля.			
				CarPRNumbers = CalculateCarPRNumbers(ZDCFAMPRNumbers, CarPRNumbersArray);
															
			} 
				catch (Exception ex)
				{
					MessageWindow.Show(ex.Message, "Constructor exception");
				}
		}
		
		// Метод для вычисления данных для адаптации. SortedList сразу сортирует словарь по ключам.
		// Ключ (string) - номер байта, Значение (UInt32).
		public SortedList<string, UInt32> GetAdaptation()
		{
			try
			{
				#region Variables Declaration
				// Результирующая переменная. Если возвращаемое значение равно null, то ZDC не имеет кодировочных данных, либо ошибка.
				SortedList<string, UInt32> RESULTAdaptationTable = new SortedList<string, uint>();
				// Переменная для проверки существования элемента.
				bool IsNodeExist;
				// Переменная для хранения значения байта.
				UInt32 WERT;
				// Переменная для хранения порядкового номера байта.
				int STELLE;
				// Переменная для хранения HEX-значения порядкового номера байта.
				string HEXSTELLE;
				// Вспомогательная переменная.
				UInt32 tmpWERT = 0;
				#endregion
				
				// Проверить все элементы TABELLE.
				if(TablesList == null)
					throw new Exception("TablesList variable cannot be null!");
				foreach (XElement Table in TablesList)
				{
					// Проверить, является ли таблица таблицей адаптации.
					XElement Mode = Table.Element("MODUS");
					if (Mode.Value == "A")
					{
						IEnumerable<XElement> TEGUEList = Table.Element("TAB").Elements("FAM").Elements("TEGUE");
						foreach (XElement TEGUE in TEGUEList)
						{							
							IEnumerable<XElement> PRNRList = TEGUE.Elements("PRNR");
							foreach (XElement PRNR in PRNRList)
							{
								// Если PR-номера соответствуют данным автомобиля, то рассчитать кодировку.
								if (CheckPRNumbers(PRNR.Value))
								{
									IEnumerable<XElement> KNOTENList = TEGUE.Elements("KNOTEN");
									foreach (XElement KNOTEN in KNOTENList)
									{
										
										// Проверить наличие элемента UEBERSPRINGEN.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "UEBERSPRINGEN");
										if (IsNodeExist) continue; // Пропустить итерацию в случае true.
										
										// Проверить наличие элемента STELLE.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "STELLE");
										if (!IsNodeExist) throw new Exception("There is no STELLE for PR-numbers: " + PRNR.Value);
										// Получить номер байта. Если возникла ошибка, то выбросить исключение.
										STELLE = Convert.ToInt32(KNOTEN.Element("STELLE").Value, 16);
										
										// Проверить наличие элемента WERT.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "WERT");
										if (!IsNodeExist) throw new Exception("There is no WERT for PR-numbers: " + PRNR.Value);
										// Получить значение бита.
										WERT = Convert.ToUInt32(KNOTEN.Element("WERT").Value, 16);
										
										// Преобразуем число в HEX-строку.
//										RealSTELLE = STELLE - 1;
										HEXSTELLE = STELLE.ToString(HexFormatString);
										// Если элемента с таким ключом еще не существует, то инициализировать его и присвоить ноль.
										if(!RESULTAdaptationTable.TryGetValue(HEXSTELLE, out tmpWERT))
										{
											RESULTAdaptationTable[HEXSTELLE] = 0;
										}
										// Записать в таблицу полученное значение.
										RESULTAdaptationTable[HEXSTELLE] = WERT;
									}
								}
							}
						}
					}
				}
				return  RESULTAdaptationTable;
			}
			catch(Exception ex)
			{
				MessageWindow.Show(ex.Message, "GetAdaption exception");
				return null;
			}
		}
		
		// Метод для вычисления калибровки.
		public List<CalibrationData> GetCalibration()
		{
			try
			{
				#region Variables Declaration
				// Результирующая переменная. Если возвращаемое значение равно null, то ZDC не имеет данных для программирования, либо ошибка.
				List<CalibrationData> RESULTCalibrationTable = new List<CalibrationData>();
				
				// Переменная для проверки существования элемента.
				bool IsNodeExist;
				// Переменная для хранения значения байта.
				byte WERT;
				// Переменная для хранения порядкового номера байта.
				int STELLE;
				// Номер позиции бита в байте.
				byte LSB = 0;
				
				// Тип калибровки.
				string MODUSTEIL = String.Empty;
				// Идентификатор сервиса.
				string serviceID = String.Empty;
				// Идентификатор адреса.
				string recordDataID = String.Empty;
				
				// Размер блока данных (для форматирования в hex).
				string dataLength = String.Empty;
				// Размер блока данных int.
				int dataListLength = -1;
				
				// Список значений байтов для каждого RDID.
				IList<byte> dataList = null;
				#endregion
				
				// Проверить все элементы TABELLE.
				if (TablesList == null)
					throw new Exception("TablesList variable cannot be null!");
				
				foreach (XElement Table in TablesList)
				{
					// Проверить, является ли таблица таблицей калибровки.
					XElement Mode = Table.Element("MODUS");
					if (Mode.Value == "K")
					{
						// Получить MODUSTEIL.
						MODUSTEIL = String.IsNullOrEmpty(Table.Element("MODUSTEIL").Value) ? null : Table.Element("MODUSTEIL").Value;
						// Получить Service ID.
						serviceID = String.IsNullOrEmpty(Table.Element("SERVICEID").Value) ? null : Table.Element("SERVICEID").Value;
						// Получить Record Data ID.
						recordDataID = String.IsNullOrEmpty(Table.Element("RDIDENTIFIER").Value) ? null : Table.Element("RDIDENTIFIER").Value;
						
						// Список для формирования значения байта. Первый параметр - номер байта, второй - его значение.
						SortedList<string, byte> tempList = new SortedList<string, byte>();
						
						// Переменная для хранения значений байтов.
						StringBuilder strBuilderData = new StringBuilder();
																																														
						IEnumerable<XElement> TEGUEList = Table.Element("TAB").Elements("FAM").Elements("TEGUE");
						foreach (XElement TEGUE in TEGUEList)
						{
							IEnumerable<XElement> PRNRList = TEGUE.Elements("PRNR");
							foreach (XElement PRNR in PRNRList)
							{
								// Если PR-номера соответствуют данным автомобиля, то рассчитать калибровку.
								if (CheckPRNumbers(PRNR.Value))
								{									
									IEnumerable<XElement> KNOTENList = TEGUE.Elements("KNOTEN");
									foreach (XElement KNOTEN in KNOTENList)
									{
//										// Проверить наличие элемента UEBERSPRINGEN.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "UEBERSPRINGEN");
										if (IsNodeExist) continue; // Пропустить итерацию в случае true.
										
										// Проверить наличие элемента STELLE.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "STELLE");
										if (!IsNodeExist) throw new Exception("There is no STELLE for PR-numbers: " + PRNR.Value); // Выбросить исключение в случае false.
										// Получить номер байта.
										STELLE = Convert.ToInt32(KNOTEN.Element("STELLE").Value, 16);
										
										// Проверить наличие элемента WERT.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "WERT");
										if (!IsNodeExist) throw new Exception("There is no WERT for PR-numbers: " + PRNR.Value); // Выбросить исключение в случае false.
										// Получить значение байта.
										WERT = Convert.ToByte(KNOTEN.Element("WERT").Value, 16);
										
										// Получить номер бита.
										// Проверить наличие элемента LSB.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "LSB");
										if (!IsNodeExist) throw new Exception("There is no LSB for PR-numbers: " + PRNR.Value); // Выбросить исключение в случае false.
										if (!Byte.TryParse(KNOTEN.Element("LSB").Value, out LSB)) throw new Exception("No LSB element or incorrect data!");
										
										// Промежуточное значение байта.
										byte tmpByte = 0;
										// Если элемента с таким ключом еще не существует, то инициализировать его и присвоить ноль.										
										string HEXSTELLE = STELLE.ToString(HexFormatString);
										if(!tempList.TryGetValue(HEXSTELLE, out tmpByte))
										{
											tempList[HEXSTELLE] = 0;
										}
										// Записать в таблицу рассчитанное значение.
										tmpByte = CalculateByte(LSB, WERT);
										tempList[HEXSTELLE] = (byte)(tempList[HEXSTELLE] + tmpByte);
									}
									// Получим список значений. Значения должны быть уже отсортированы по ключу.
									dataList = tempList.Values;
									// Получим длину списка значений.
									dataListLength = dataList.Count;
									dataLength = dataListLength.ToString("X");
								}
							}
						}
						// Сформируем объект StringBuilder из значений.
						foreach (byte element in dataList)
						{
							string hexElement = String.Format("0x{0:X2},", element);
							strBuilderData.Append(hexElement);
						}
						
						// Перевести StringBuilder в строку и удалить последнюю запятую.
						string data = strBuilderData.ToString();
						char[] charsToTrim = {','};
						data = data.TrimEnd(charsToTrim);
						
						// Пропустить итерацию, если длина данных равна нулю. То есть данных нет и нет смысла создавать объект.
						if (dataListLength == 0) continue;
						
						// Создать новый объект CalibrationData.
						CalibrationData calibData = new CalibrationData(MODUSTEIL, serviceID, recordDataID, data, dataLength);
						// Добавить его в список.
						RESULTCalibrationTable.Add(calibData);
						tempList = null;
						strBuilderData = null;
					}
				}
				return RESULTCalibrationTable;
			}
			catch(Exception ex)
			{
				MessageWindow.Show(ex.Message, "GetCalibration exception");
				return null;
			}
		}
		
		// Метод для вычисления кодирования (длинное кодирование). SortedList сразу сортирует словарь по ключам.
		// Ключ (string) - номер байта, Значение (byte).
		public SortedList<string, byte> GetCoding(out bool IsShortCoding, out byte[] ShortCodingValues)
		{
			try
			{
			#region Variables Declaration
				// Переменная, содержащая таблицу кодирования в формате байт-значение.
				SortedList<string, byte> RESULTCodingTable = new SortedList<string, byte>();
				// Переменная для проверки существования элемента.
				bool IsNodeExist;
				// Переменная для хранения значения байта.
				byte WERT;
				// Номер позиции бита в байте.
				byte LSB = 0;
				// Переменная для хранения порядкового номера байта.
				int STELLE;
				// В ZDC-файле номера байтов хранятся как ПорядковыйНомерБайта + 1, поэтому нужно привести к нормальному виду.
				int RealSTELLE = 0;
				// Переменная для хранения HEX-значения порядкового номера байта.
				string HEXSTELLE;
				// Переменная принимает значение true, если кодирование короткое.
				bool _IsShortCoding = false;
				// Вспомогательная переменная.
				byte tmpByte = 0;
			#endregion
				
				// Проверить все элементы TABELLE.
				if(TablesList == null)
					throw new Exception("TablesList variable cannot be null!");
				foreach (XElement Table in TablesList)
				{
					// Проверить, является ли таблица таблицей кодировки.
					XElement Mode = Table.Element("MODUS");
					if (Mode.Value == "C")
					{
						IEnumerable<XElement> TEGUEList = Table.Element("TAB").Elements("FAM").Elements("TEGUE");
						// Проверить, короткое кодирование или длинное. Если ZAHLENBASIS равен 256 или 10, то кодирование короткое.
						_IsShortCoding = ((Table.Element("ZAHLENBASIS").Value == "256") || (Table.Element("ZAHLENBASIS").Value == "10"));
						foreach (XElement TEGUE in TEGUEList)
						{
							IEnumerable<XElement> PRNRList = TEGUE.Elements("PRNR");
							foreach (XElement PRNR in PRNRList)
							{
								// Если PR-номера соответствуют данным автомобиля, то рассчитать кодировку.
								if (CheckPRNumbers(PRNR.Value))
								{
									IEnumerable<XElement> KNOTENList = TEGUE.Elements("KNOTEN");
									foreach (XElement KNOTEN in KNOTENList)
									{
										// Проверить наличие элемента UEBERSPRINGEN.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "UEBERSPRINGEN");
										if (IsNodeExist) continue; // Пропустить итерацию в случае true.
										
										// Проверить наличие элемента STELLE.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "STELLE");
										if (!IsNodeExist) throw new Exception("There is no STELLE for PR-numbers: " + PRNR.Value); // Выбросить исключение в случае false.
										// Получить номер байта. Если возникла ошибка, то выбросить исключение.
										STELLE = Convert.ToInt32(KNOTEN.Element("STELLE").Value, 16);
										
										// Получить номер бита.
										// Проверить наличие элемента LSB.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "LSB");
										if (!IsNodeExist) throw new Exception("There is no LSB for PR-numbers: " + PRNR.Value); // Выбросить исключение в случае false.
										if (!Byte.TryParse(KNOTEN.Element("LSB").Value, out LSB)) throw new Exception("No LSB element or incorrect data!");
										
										// Проверить наличие элемента WERT.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "WERT");
										if (!IsNodeExist) throw new Exception("There is no WERT for PR-numbers: " + PRNR.Value); // Выбросить исключение в случае false.
										// Получить значение байта.
										WERT = Convert.ToByte(KNOTEN.Element("WERT").Value, 16);
										
										// В случае короткого кодирования не изменять значения STELLE.
										if (_IsShortCoding)
											RealSTELLE = STELLE;
										else
											RealSTELLE = STELLE - 1;
										// Преобразуем число в HEX-строку.
										HEXSTELLE = RealSTELLE.ToString(HexFormatString);
										// Если элемента с таким ключом еще не существует, то инициализировать его и присвоить ноль.
										if(!RESULTCodingTable.TryGetValue(HEXSTELLE, out tmpByte))
										{
											RESULTCodingTable[HEXSTELLE] = 0;
										}
										// Записать в таблицу рассчитанное значение.
										tmpByte = CalculateByte(LSB, WERT);
										RESULTCodingTable[HEXSTELLE] = (byte)(RESULTCodingTable[HEXSTELLE] + tmpByte);
									}
								}
							}
						}
					}
				}
				// Присвоить значение выходной переменной, которая определяет короткое кодирование или нет.
				IsShortCoding = _IsShortCoding;
				if (_IsShortCoding)
				{					
					// Получить значения байтов.
					IList<byte> CodingValues = RESULTCodingTable.Values;
					// Перевернуть полученные значения.
					IEnumerable<byte> ReversedValues = CodingValues.Reverse();
					// Преобразовать в массив.
					ShortCodingValues = ReversedValues.ToArray();
					// Вернуть null вместо SortedList.
					return null;
				}
				ShortCodingValues = null;
				return RESULTCodingTable;
			}
			catch(Exception ex)
			{
				MessageWindow.Show(ex.Message, "GetCoding exception");
				// Присвоить значение выходной переменной, которая определяет короткое кодирование или нет.
				IsShortCoding = false;
				ShortCodingValues = null;
				return null;
			}
		}
		
		// Метод для вычисления программирования.
		public List<ProgrammingData> GetProgramming()
		{
			try
			{
				#region Variables Declaration
				// Результирующая переменная. Если возвращаемое значение равно null, то ZDC не имеет данных для программирования, либо ошибка.
				List<ProgrammingData> RESULTProgrammingTable = new List<ProgrammingData>();
				
				// Переменная для проверки существования элемента.
				bool IsNodeExist;
				// Переменная для хранения значения байта.
				string WERT = String.Empty;
				// Переменная для хранения порядкового номера байта.
				int STELLE;
				// Переменная для хранения HEX-значения порядкового номера байта.
				string HEXSTELLE;
				#endregion
				
				// Проверить все элементы TABELLE.
				if (TablesList == null)
					throw new Exception("TablesList variable cannot be null!");
				
				foreach (XElement Table in TablesList)
				{
					// Проверить, является ли таблица таблицей программирования.
					XElement Mode = Table.Element("MODUS");
					// Получить MODUSTEIL.
					string MODUSTEIL = String.IsNullOrEmpty(Table.Element("MODUSTEIL").Value) ? null : Table.Element("MODUSTEIL").Value;
					if (Mode.Value == "P")
					{
						IEnumerable<XElement> TEGUEList = Table.Element("TAB").Elements("FAM").Elements("TEGUE");
						foreach (XElement TEGUE in TEGUEList)
						{
							IEnumerable<XElement> PRNRList = TEGUE.Elements("PRNR");
							foreach (XElement PRNR in PRNRList)
							{
								// Если PR-номера соответствуют данным автомобиля, то рассчитать кодировку.
								if (CheckPRNumbers(PRNR.Value))
								{
									IEnumerable<XElement> KNOTENList = TEGUE.Elements("KNOTEN");
									foreach (XElement KNOTEN in KNOTENList)
									{
										// Проверить наличие элемента UEBERSPRINGEN.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "UEBERSPRINGEN");
										if (IsNodeExist) continue; // Пропустить итерацию в случае true.
										
										// Проверить наличие элемента STELLE.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "STELLE");
										if (!IsNodeExist) // Выбросить исключение в случае false.
											throw new Exception("There is no STELLE for PR-numbers: " + PRNR.Value);
										
										// Проверить наличие элемента WERT.
										IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "WERT");
										if (!IsNodeExist)
										{
											// Проверить наличие данных для текущего STELLE.
											IsNodeExist = KNOTEN.Elements().Any(node => node.Name == "DATEN-NAME");
											if (IsNodeExist)
											{
												string DataName = KNOTEN.Element("DATEN-NAME").Value;
												STELLE = GetStartAddress(DataName);
												if (STELLE == Int32.MinValue)
													throw new Exception("Cannot get STELLE value");
												// Преобразуем число в HEX-строку.
												HEXSTELLE = STELLE.ToString(HexFormatString);
												WERT = GetPRGString(DataName);
												string strDataLength = GetDataLengthString(DataName);
												string strDataFormat = GetDataFormatString(DataName);
												
												ProgrammingData progData = new ProgrammingData(MODUSTEIL, HEXSTELLE, DataName, WERT, strDataLength, strDataFormat, true);
												RESULTProgrammingTable.Add(progData);
											}
											else
												continue; // Пропустить итерацию в случае false.
										}
										else
										{
											// Получить номер байта.
											STELLE = Convert.ToInt32(KNOTEN.Element("STELLE").Value, 16);
											// Получить значение бита.
											WERT = KNOTEN.Element("WERT").Value;
											// Преобразуем число в HEX-строку.
											HEXSTELLE = STELLE.ToString(HexFormatString);
											// Создать новый объект.
											ProgrammingData progData = new ProgrammingData(MODUSTEIL, HEXSTELLE, WERT);
											// Добавить его в список.
											RESULTProgrammingTable.Add(progData);
										}
									}
								}
							}
						}
					}
				}
				return RESULTProgrammingTable;
			}
			catch(Exception ex)
			{
				MessageWindow.Show(ex.Message, "GetProgramming exception");
				return null;
			}
		}
		
		// Метод (необязательный) для получения диагностической информации из ZDC-файла.
		public DiagInformation GetDiagInformation()
		{
			try
			{
				if (ZDCFile != null)
				{					
					IEnumerable<XElement> DiagInfoTable = ZDCFile.Root.Element("VORSCHRIFT").Element("DIREKT").Element("DIAGN").Elements();
					string addr = null;
					string bus = null;
					string kwp = null;
					string tp = null;
					foreach (XElement node in DiagInfoTable)
					{
						switch (node.Name.ToString())
						{
							case "ADR":
								addr = node.Value;
								break;
							case "BUS":
								bus = node.Value;
								break;
							case "KWP":
								kwp = node.Value;
								break;
							case "TP":
								tp = node.Value;
								break;
						}
					}
					DiagInformation RESULT_DiagInfo = new DiagInformation(addr, bus, kwp, tp);
					return RESULT_DiagInfo;
				}
				return null;
			}
			catch (Exception ex)
			{
				MessageWindow.Show(ex.Message, "GetDiagInformation exception");
				return null;
			}
		}
		
		// Метод (необязательный) для получения информации о ZDC-файле.
		public ZDCInformation GetZDCInformation()
		{
			try
			{
				if (ZDCFile != null)
				{
					ZDCInformation RESULT_ZDCInfo = null;
					IEnumerable<XElement> DiagInfoTable = ZDCFile.Root.Element("IDENT").Elements();
					string dataName = null;
					string dataID = null;
					string stylesheetVersion = null;
					string version = null;
					string date = null;
					string description = null;
					string checksum = null;
					foreach (XElement node in DiagInfoTable)
					{
						switch (node.Name.ToString())
						{
							case "DATEINAME":
								dataName = node.Value;
								break;
							case "DATEIID":
								dataID = node.Value;
								break;
							case "VERSION-TYP":
								stylesheetVersion = node.Value;
								break;
							case "VERSION-INHALT":
								version = node.Value;
								break;
							case "DATUM":
								date = node.Value;
								break;
							case "BESCHREIBUNG":
								description = node.Value;
								break;
							case "CNT-CHECKSUMME":
								checksum = node.Value;
								break;
						}
					}
					RESULT_ZDCInfo = new ZDCInformation(dataName, dataID, stylesheetVersion, version, date, description, checksum);
					return RESULT_ZDCInfo;
				}
				return null;
			}
			catch (Exception ex)
			{
				MessageWindow.Show(ex.Message, "GetDiagInformation exception");
				return null;
			}
		}

		// Освободить ресурсы.
		public void Dispose()
    	{
			CarPRNumbers = null;
			ZDCFile = null;
    	}
	}
	
	/// <summary>
	/// Класс, который содержит данные для программирования.
	/// </summary>
	public class ProgrammingData
	{
		#region Public properties
		public string ModusTeil { get; private set; }
		
		public string ByteNumber { get; private set; }
		
		public string ByteValue { get; private set; }
		
		public string Data { get; private set; }
		
		public string DataLength { get; private set; }
		
		public string DataFormat { get; private set; }
		// Свойство для отображения или скрытия RowDetails во View.
		public bool HasData { get; private set; }
		#endregion
	
		#region Class constructors
		public ProgrammingData(string modusTeil, string byteNumber, string byteValue, string data, string dataLength, string dataFormat, bool hasData)
		{
			this.ModusTeil = modusTeil;
			this.ByteNumber = byteNumber;
			this.ByteValue = byteValue;
			this.Data = data;
			this.DataLength = dataLength;
			this.DataFormat = dataFormat;
			this.HasData = hasData;
		}
		
		public ProgrammingData(string modusTeil, string byteNumber, string byteValue)
			: this(modusTeil, byteNumber, byteValue, null, null, null, false)
		{
			this.ModusTeil = modusTeil;
			this.ByteNumber = byteNumber;
			this.ByteValue = byteValue;
		}
		#endregion
	}
	
	/// <summary>
	/// Класс, который содержит данные для калибровки.
	/// </summary>
	public class CalibrationData
	{
		#region Public properties
		public string ModusTeil { get; private set; }
		
		public string ServiceID { get; private set; }
		
		public string RecordDataID { get; private set; }
		
		public string Data { get; private set; }
		
		public string DataLength { get; private set; }
		#endregion
	
		#region Class constructors
		public CalibrationData()
		{
			this.ModusTeil = null;
			this.ServiceID = null;
			this.RecordDataID = null;
			this.Data = null;
			this.DataLength = null;
		}
		
		public CalibrationData(string modusTeil, string serviceID, string recordDataID, string data, string dataLength)
		{
			this.ModusTeil = modusTeil;
			this.ServiceID = serviceID;
			this.RecordDataID = recordDataID;
			this.Data = data;
			this.DataLength = dataLength;
		}
		#endregion
	}
	
	/// <summary>
	/// Класс, содержащий информацию о ZDC-файле.
	/// </summary>
	public class ZDCInformation
	{
	#region Public properties
		public string DataName { get; private set; }
		
		public string DataID { get; private set; }
		
		public string StylesheetVersion { get; private set; }
		
		public string Version { get; private set; }
		
		public string Date { get; private set; }
		
		public string Description { get; private set; }
		
		public string Checksum { get; private set; }
	#endregion
	
	#region Class constructors
		public ZDCInformation()
		{
			this.DataName = null;
			this.DataID = null;
			this.StylesheetVersion = null;
			this.Version = null;
			this.Date = null;
			this.Description = null;
			this.Checksum = null;
			
		}
	
		public ZDCInformation(string dataName, string dataID, string stylesheetVersion, string version, string date, string description, string checksum)
		{
			this.DataName = dataName;
			this.DataID = dataID;
			this.StylesheetVersion = stylesheetVersion;
			this.Version = version;
			this.Date = date;
			this.Description = description;
			this.Checksum = checksum;
		}
	#endregion
	}
	
	/// <summary>
	/// Этот класс возвращается необязательным методом GetDiagInformation.
	/// Содержит дополнительную информацию из ZDC-файла.
	/// </summary>
	public class DiagInformation
	{
		#region Public properties
		public string Address { get; private set; }
		public string Bus { get; private set; }
		public string KeywordProtocol { get; private set; }
		public string TransportProtocol { get; private set; }
		#endregion
		
		#region Class constructors
		public DiagInformation()
		{
			this.Address = null;
			this.Bus = null;
			this.KeywordProtocol = null;
			this.TransportProtocol = null;
		}
		
		public DiagInformation(string address, string bus, string keywordProtocol, string transportProtocol)
		{
			this.Address = address;
			this.Bus = bus;
			this.KeywordProtocol = keywordProtocol;
			this.TransportProtocol = transportProtocol;
		}
		#endregion
	}
}
