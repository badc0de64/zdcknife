﻿/*
 * Created by SharpDevelop.
 * User: DZC1KAZ
 * Date: 08.12.2015
 * Time: 10:05
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace ZDCknife
{/// <summary>
	/// Interaction logic for MessageWindow.xaml
	/// </summary>
	public partial class MessageWindow : Window
	{
		public MessageWindow()
		{
			InitializeComponent();
		}
		
		public static bool Show(string message, string caption)
		{
			try
			{
				MessageWindow msgWnd = new MessageWindow();
				msgWnd.txtBlockCaption.Text = caption;
				msgWnd.txtBoxMessage.Text = message;
				msgWnd.Owner = Application.Current.MainWindow;
				msgWnd.ShowDialog();
				return true;
			}
			catch
			{
				return false;
			}
		}
		void txtBlockCaption_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			this.DragMove();
		}
		void Button_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}
	}
}