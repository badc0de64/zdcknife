﻿/*
 * Created by SharpDevelop.
 * User: dzc1kaz
 * Date: 16.10.2015
 * Time: 20:31
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows;
using Microsoft.Win32;
using ZDCknife.Classes;
using System.Collections.Generic;
using System.Linq;

namespace ZDCknife
{
	public partial class MainWindow : Window
	{
	
		string fileName;
		
		public MainWindow()
		{
			InitializeComponent();
		}
		void btnOpenZDCFile_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "XML document|*.xml|All Files|*.*";
			
			if(openFileDialog.ShowDialog() == true)
			{
				fileName = openFileDialog.FileName;
				txtBlockFileName.Text = fileName;
					
				btnProcess.IsEnabled = true;
			}
		}
		void BtnProcess_Click(object sender, RoutedEventArgs e)
		{
//			try
//			{
				if (String.IsNullOrEmpty(fileName))
					throw new Exception("ZDC file is not opened!");
				ZDCConverter ZDCConvObject = new ZDCConverter(fileName, txtBoxPRNumbers.Text);
				string strValues = String.Empty;
				string strBytes = String.Empty;
				if (ZDCConvObject != null & ZDCConvObject.CarPRNumbers != null)
				{

#region GetAdaptation
					// Очистить и обновить таблицу.
					dgAdaptationTable.ItemsSource = null;
					dgAdaptationTable.Items.Refresh();
					if (ZDCConvObject.HasAdaptationData)
					{
						SortedList<string, UInt32> AdaptionValues = ZDCConvObject.GetAdaptation();
						if (AdaptionValues != null)
						{
							if (AdaptionValues.Count > 0)
							{
								IList<string> Keys = AdaptionValues.Keys;
								SortedList<string, string> HEXAdaptionValues = new SortedList<string, string>();
		
								UInt32 tmpVal;
								foreach (string Key in Keys)
								{
									AdaptionValues.TryGetValue(Key, out tmpVal);
									HEXAdaptionValues.Add(Key, tmpVal.ToString("X"));
								}
								
								dgAdaptationTable.Visibility = Visibility.Visible;
								dgAdaptationTable.ItemsSource = HEXAdaptionValues;
							}
						}
					}
#endregion

#region GetCalibration
					// Очистить и обновить таблицу.
					dgCalibTable.ItemsSource = null;
					dgCalibTable.Items.Refresh();
					if (ZDCConvObject.HasCalibrationData)
					{
						List<CalibrationData> CalibrationValues = ZDCConvObject.GetCalibration();
						if (CalibrationValues != null)
						{
							if (CalibrationValues.Count > 0)
							{
								dgCalibTable.Visibility = Visibility.Visible;
								dgCalibTable.ItemsSource = CalibrationValues;
							}
						}
					}
#endregion

#region GetCoding
					// Очистить и обновить таблицу.
					dgCodingTable.ItemsSource = null;
					dgCodingTable.Items.Refresh();
					if (ZDCConvObject.HasCodingData)
					{
						SortedList<string, string> HEXCodingValues = new SortedList<string, string>();
						bool isShortCoding;
						byte[] ShortCoding = null;
						string strShortCoding = String.Empty;
						SortedList<string, byte> CodingValues = ZDCConvObject.GetCoding(out isShortCoding, out ShortCoding);
						if (CodingValues != null)
						{
							if (CodingValues.Count > 0)
							{
								IList<string> Keys = CodingValues.Keys;
										
								byte tmpByte;
								foreach (string Key in Keys)
								{
									CodingValues.TryGetValue(Key, out tmpByte);
									string StrtmpByte = tmpByte.ToString("X2");
									HEXCodingValues.Add(Key, StrtmpByte);
								}
							}
						}
						else if (isShortCoding)
						{
							foreach (byte ShortCodingValue in ShortCoding)
							{
								strShortCoding += ShortCodingValue.ToString("X2");
							}
							
							HEXCodingValues.Add("Short coding", strShortCoding);
						}
						
						dgCodingTable.Visibility = Visibility.Visible;
						dgCodingTable.ItemsSource = HEXCodingValues;
					}
#endregion
			
#region GetProgramming
					// Очистить и обновить таблицу.
					dgProgTable.ItemsSource = null;
					dgProgTable.Items.Refresh();
					if (ZDCConvObject.HasProgrammingData)
					{
						List<ProgrammingData> ProgrammingValues = ZDCConvObject.GetProgramming();
						if (ProgrammingValues != null)
						{
							if (ProgrammingValues.Count > 0)
							{
								dgProgTable.Visibility = Visibility.Visible;
								dgProgTable.ItemsSource = ProgrammingValues;
							}
						}
					}
#endregion

#region GetZDCInformation
					ZDCInformation zdcInfo = ZDCConvObject.GetZDCInformation();
					if (zdcInfo != null)
					{
						ZDCInfoGrid.DataContext = zdcInfo;
					}
#endregion

#region GetDiagInformation
//					DiagInformation? diagInfo = new DiagInformation?();
//					diagInfo = ZDCConvObject.GetDiagInformation();
//					if (diagInfo != null)
//					{
//						txtBoxAddr.Text = diagInfo.Value.Address;
//						txtBoxBus.Text = diagInfo.Value.Bus;
//						txtBoxKWP.Text = diagInfo.Value.KeywordProtocol;
//						txtBoxTP.Text = diagInfo.Value.TransportProtocol;						
//					}
#endregion
				}
//			}
//			catch (Exception ex)
//			{
//				MessageWindow.Show(ex.Message, "Process exception");
//			}
		}
	}
}